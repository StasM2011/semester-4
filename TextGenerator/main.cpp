#include <iostream>
#include <map>
#include <deque>
#include <string>
#include <fstream>
#include <locale>

using namespace std;

typedef deque<string> prefix;

const int N = 1;
int Words = 0;
map<prefix, deque<string>> db;

void build(prefix&, ifstream&);
void add(prefix&, string);

int main() {
	setlocale(LC_ALL, "Russian");
	
	deque<string> poem;

	ifstream file("data.txt");
	prefix p;
	build(p, file);

	prefix p1;
	p1.push_back("�");
	for(int i = 0; i < Words; i++) {
		deque<string>& d = db[p1];
		if(d.empty())
			continue;
		string s = d[rand() % d.size()];
		cout << s << " ";
		p1.pop_front();
		p1.push_back(s);
	}

	getchar();
	return 0;
}

void build(prefix& p, ifstream& in) {
	string buf;
	while(in >> buf)
		add(p, buf);
}

void add(prefix& p, string s) { 
	if(p.size() == N) {
		db[p].push_back(s);
		p.pop_front();
	}
	p.push_back(s);
	Words++;
}