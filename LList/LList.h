#pragma once

struct Country {
	char* UN;
	char* iso2;
	char* iso3;
	char* name;
	char* capital;
};

typedef Country DataType;

class LList {
	struct ITEM {
		DataType* data;
		ITEM* next;
	};
	LList::ITEM* create(const DataType&);
	ITEM* head;
	ITEM* tail;
public:
	LList():head(0), tail(0){};
	LList(const DataType&);
	~LList();
	void addHead(const DataType&);
	void addTail(const DataType&);
	DataType rmHead();
	void print() const;
	void initFirst(const DataType&);
	DataType* findCountry(char*);
};