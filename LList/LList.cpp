#include "LList.h"
#include <iostream>

using namespace std;

LList::ITEM* LList::create(const DataType& data) {
	ITEM* item = new ITEM();
	item->data = new DataType();
	*item->data = data;
	item->next = 0;
	return item;
}

void LList::initFirst(const DataType& data) {
	head = create(data);
	tail = head;
}

LList::LList(const DataType& data) {
	initFirst(data);
}

LList::~LList() {

}

void LList::addTail(const DataType& data) {
	if(head&&tail) {
		tail->next = create(data);
		tail = tail->next;
	} else initFirst(data);
}

void LList::addHead(const DataType& data) {
	if(head&&tail) {
		ITEM* temp = create(data);
		temp->next = head;
		head = temp;
	} else initFirst(data);
}

void LList::print() const {
	ITEM* temp = head;
	while(temp) {
		//cout << *(temp->data) << endl;
		temp = temp->next;
	}
}

DataType* LList::findCountry(char* name) {
	ITEM* temp = head;
	while(temp) {
		if(!strcmp(name, temp->data->name))
			return temp->data;
		temp = temp->next;
	}
	return 0;
}