#include "LList.h"
#include <iostream>
#include <fstream>
#include <Windows.h>

using namespace std;

Country* parseCountry(char* buf);

int main() {
	LList list;
	ifstream fin;
	fin.open("countries.csv");
	if(!fin.fail()) {
		char buf[256];
		while(!fin.eof()) {
			fin.getline(buf, 256);
			Country* c = parseCountry(buf);
			list.addTail(*c);
		}
		fin.close();
	}
	if(Country* c = list.findCountry("Afghanistan"))
		cout << c->capital;
	getchar();
	return 0;
}

Country* parseCountry(char* buf) {
	Country* country = new Country();
	int index = 0;
	for(int i = 0; i < 5; i++) {
		char* b = new char[30];
		int bi = 0;
		while(buf[index] && buf[index] != ',')
			b[bi++] = buf[index++];
		b[bi] = '\0';
		index++;
		switch(i) {
		case 0:
			country->UN = b;
			break;
		case 1:
			country->iso2 = b;
			break;
		case 2:
			country->iso3 = b;
			break;
		case 3:
			country->name = b;
			break;
		case 4:
			country->capital = b;
			break;
		}
	}
	return country;
}