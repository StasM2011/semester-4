#include <iostream>
#include <fstream>
#include <string>
#include <locale>

using namespace std;

typedef string DataType;

struct Node {
	DataType data;
	Node* left;
	Node* right;
	int count;
};

void print(const Node* root);
Node* addNode(DataType& data, Node* root);
Node* find(const DataType& value, Node *root);
void printByFreq(const Node* root);

int main() {
	setlocale(LC_ALL, "Russian");
	ifstream file("data.txt");
	string buf;
	Node root;
	file >> buf;
	root.data = buf;
	root.left = 0;
	root.right = 0;
	while(file >> buf)
		addNode(buf, &root);
	Node* n = find("�����", &root);
	if(n)
		cout << n->count;
	else
		cout << "�� �������";
	getchar();
	//print(&root);
	//getchar();
	printByFreq(&root);
	return 0;
}

void print(const Node* root) {
	if(root) {
		print(root->left);
		cout << root->data << endl;
		print(root->right);
	}
}

void printByFreq(const Node* root) {

}

Node* find(const DataType& value, Node *root) {
	if(!root) 
		return 0;
	if(value == root->data)
		return root;
	else
		return find(value, (value < root->data ? root->left : root->right));
}

Node* addNode(DataType& data, Node* root) {
	if(!root) {
		root = new Node();
		root->data = data;
		root->left = 0;
		root->right = 0;
		root->count = 1;
	} else if(root->data > data)
		root->left = addNode(data, root->left);
	else if(root->data < data)
		root->right = addNode(data, root->right);
	else
		root->count++;
	return root;
}