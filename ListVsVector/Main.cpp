#include <iostream>
#include <time.h>
#include <fstream>
#include <locale>
#include <list>
#include <vector>
#include <math.h>

using namespace std;

int main() {
	const int CHAR_NUM = 1000;

	setlocale(LC_ALL, "Russian");

	ifstream file;
	
	list<char> lcoll;
	vector<char> vcoll;

	file.open("data.txt");
	while(!file.eof()) {
		char c = file.get();
		lcoll.push_back(c);
		vcoll.push_back(c);
	}
	file.close();

	int clockStart = clock();
	for(int i = 0; i < CHAR_NUM; i++)
		lcoll.push_front((char) rand());
	cout << "List " << clock() - clockStart << endl;

	clockStart = clock();
	for(int i = 0; i < CHAR_NUM; i++)
		vcoll.insert(vcoll.begin(), (char) rand());
	cout << "Vector " << clock() - clockStart << endl;
	
	getchar();
	return 0;
}